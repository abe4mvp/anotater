class UploadController < ApplicationController
  def index

  end

  def create
    uploaded_io = params[:upload]
    directory = "public/CodeFiles"
    File.open(Rails.root.join(directory, uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
    render :text => "File has been uploaded successfully"
  end

  def show
    #show a file that has been uploaded
  end

end
